description = "Foo API specification"

val openApi: Configuration by configurations.creating {
    isCanBeConsumed = true
    isCanBeResolved = false
}


artifacts {
    add("openApi", file(layout.projectDirectory.file("contract/spec.yaml")))
}

