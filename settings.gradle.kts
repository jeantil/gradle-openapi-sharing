pluginManagement {
    plugins {        
        kotlin("jvm") version "1.8.21"
        kotlin("plugin.spring") version "1.8.21"
        id("org.openapi.generator") version "6.6.0"
    }

    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
}


rootProject.name = "openapi-multiproject"

include("api")
include("api-client")
include("api-contract")