# Open api contracts as gradle projects

The [openapi gradle generator plugin](https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-gradle-plugin) allows creating server or client flavors of code based on a single contract file. However the sample documentation only shows how to use manually defined relative paths to let the generator find the specification. Within the context of a large mono repo where tens or hundreds of developpers contribute, the need to move things around makes such hard coded relative paths fairly brittle. 

In this repository we leverage gradle's [cross project publicaton](https://docs.gradle.org/current/userguide/cross_project_publications.html#sec:simple-sharing-artifacts-between-projects) to introduce a stable logical reference (the name of the project holding the contract) that can then be used to derive the necessary filesystem paths. 

## Structure of the project 

```
├── api
│  └── build.gradle.kts
├── api-client
│  └── build.gradle.kts
├── api-contract
│  ├── build.gradle.kts
│  └── contract
│     ├── common.yaml
│     ├── foo.yaml
│     └── spec.yaml
├── build.gradle.kts
└── settings.gradle.kts
```

## Producer : publishing the `output` of the contract project

We are going to _cheat_ a bit by saying that the output of the contract project is the spec itself. It is not actually the output of a task since the spec is manually defined but it is the artifact that we want to publish.

```
//api-contract/build.gradle.kts
val openApi: Configuration by configurations.creating {
    isCanBeConsumed = true
    isCanBeResolved = false
}


artifacts {
    add(openApi.name, file(layout.projectDirectory.file("contract/spec.yaml")))
}
```

We start by defining a [consumable configuration](https://docs.gradle.org/current/userguide/declaring_dependencies.html#sec:resolvable-consumable-configs). 

We then add the specification file as the artifact for this configuration. It is important to note that artifacts can only be files and not directories. While in theory we can add multiple files to the configuration output, the rest of this sample only works when there is a single one.

