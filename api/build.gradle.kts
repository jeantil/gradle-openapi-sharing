plugins{
    `java-library`
    kotlin("jvm")
    kotlin("plugin.spring")
    id("org.openapi.generator")
}

 val openApiConf by configurations.creating {
     isCanBeConsumed = false
 }

dependencies{
    openApiConf(
        project(
            mapOf(
                "path" to ":api-contract",
                "configuration" to "openApi"
            )
        )
    )
}

openApiGenerate {
    generatorName.set("kotlin-spring")
    outputDir.set("$buildDir/generated-source/openapi/src/main/kotlin")
    cleanupOutput.set(true)
    configOptions.set(
            mapOf(
                    "delegatePattern" to "true",
                    // will generate a different API interface for each tag
                    "useTags" to "true",
                    "sourceFolder" to "",
                    "enumPropertyNaming" to "UPPERCASE"
            )
    )
    globalProperties.set(
            mapOf(
                    "supportingFiles" to "ApiUtil.kt",
                    "apis" to "",
                    "models" to ""
            )
    )

    inputSpec.set(provider { openApiConf.singleFile.path })
}

sourceSets {
    main {
        java.srcDir(tasks.openApiGenerate)
    }
}

kotlin {
    sourceSets {
        main {
            kotlin.srcDir(tasks.openApiGenerate)
        }
    }
}

 tasks.openApiGenerate {
     val sharedFiles: FileCollection = openApiConf
     // ensures the task depends on all open api fragments next to the contract
     //inputs.dir(file("$projectDir/../api-contract/contract/"))
    inputs.dir(file(sharedFiles.asPath).parentFile)
     
 }
