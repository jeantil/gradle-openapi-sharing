plugins{
    `java-library`
    kotlin("jvm")
    kotlin("plugin.spring")
    id("org.openapi.generator")
}

 val openApiConf by configurations.creating {
     isCanBeConsumed = false
 }

dependencies{
    openApiConf(
        project(
            mapOf(
                "path" to ":api-contract",
                "configuration" to "openApi"
            )
        )
    )
}

openApiGenerate {
    group = "openapi tools"
    generatorName.set("kotlin-spring")
    globalProperties.set(mapOf(
             "models" to "",
             "apis" to ""
    ))
    configOptions.set(mapOf(
             "library" to "spring-cloud",
             "sourceFolder" to "",
             "enumPropertyNaming" to "UPPERCASE",
             "apiSuffix" to "Ws",
             "serializationLibrary" to "jackson",
             "interfaceOnly" to "true",
    ))

    outputDir.set("$buildDir/generated-source/openapi-ws-client/src/main/kotlin")
    cleanupOutput.set(true)
    ignoreFileOverride.set("$projectDir/.openapi-generator-ignore")
    packageName.set("com.foo.generated.ws")
    inputSpec.set(provider { openApiConf.singleFile.path })
}

tasks.openApiGenerate {
     val sharedFiles: FileCollection = openApiConf
     
     // ensures the task depends on all open api fragments next to the contract
     inputs.dir(file(sharedFiles.asPath).parentFile)
     
     doFirst{
        logger.lifecycle("sharedfiles as path {}", sharedFiles.asPath)
     }
 }

sourceSets {
    main {
        java.srcDir(tasks.openApiGenerate)
    }
}

kotlin {
    sourceSets {
        main {
            kotlin.srcDir(tasks.openApiGenerate)
        }
    }
}
